.. Bourdillat-Adjoudj documentation master file, created by
   sphinx-quickstart on Sun Feb 26 08:56:06 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue dans la documentation de KNN
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   modules
   KNN


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
