# KNN

Ce package python permet de classer des documents texte bruts de manière supervisée, en fournissant
au système les documents déjà classés. Il est alors possible de fournir un ou plusieurs fichiers dont
le système tentera de définir la classe la plus probable, basée sur une approche vectorielle des textes
(statistique).  

Le package se veut simple d'utilisation mais aussi configurable, en permettant à l'utilisateur de choisir
voire d'écrire une fonction de normalisation ou encore de similarité, et aussi de fournir une stoplist au besoin.  

La documentation complète du projet est disponible ici : [***knn***](https://bourdije.gricad-pages.univ-grenoble-alpes.fr/knn/)

![Organisation du package](/uml/packages_KNN.svg "organigramme du package"){: .shadow}

[TOC]

## Utilisation

### Démarrage rapide

Le package intègre une classe `Classifier` qui permet de facilement mettre en place un système fonctionnel.  
Un objet `Classifier` gère tout seul les classes et met en oeuvre des fonctions permettant des traitements 
groupés.  

Ainsi, pour générer des classes depuis des dossiers (en utilisant le nom des dossiers comme *label*) on peut faire :

```python
stoplist = "STOPLIST.txt" # La stoplist peut être un nom de fichier ou une liste / un set de tokens
classifier = KNN.Classifier("Test de classification en k-ppv.", stoplist=stoplist, hapax=True) # Le premier argument est une description du système

classifier.classes_from_directories("textes") # On génère des classes depuis un dossier, chaque sous-dossier étant une classe et les fichiers le composant, des vecteurs
```

Les méthodes `classes_from_directories()` et `classes_from_directory()` sont utiles quand l'utilisateur a déjà une arborescence représentant la structure de ses classes.  

Pour classer des fichiers, on peut ensuite faire :

```python
# arguments : dossier(s) contenant les fichiers, k, fonction de similarité, fonction de normalisation
results = classifier.classify_from_directories("textes", 5, KNN.cosVector, normalize=KNN.tf_idf)
```

L'objet renvoyé est un dictionnaire contenant les métadonnées du traitement ainsi que les résultats détaillés pour chaque texte. Par exemple, pour classer 
deux textes 'les_fleurs_du_mal.txt' et 'TAL-2008-49-3-01-Adda.txt' tous les deux situés dans 'textes', on obtiendra :

```json
{
	"method": "cosVector",
	"k": 5,
	"normalization": "tf_idf",
	"textes\\les_fleurs_du_mal.txt": {
		"litte": {
			"votes": 3,
			"%_votes": 0.375,
			"sum": 2.2391753442958935,
			"average": 0.7463917814319645
		},
		"tal": {
			"votes": 1,
			"%_votes": 0.125,
			"sum": 0.6857275752303571,
			"average": 0.6857275752303571
		},
		"eco": {
			"votes": 1,
			"%_votes": 0.125,
			"sum": 0.6622298183121857,
			"average": 0.6622298183121857
		}
	},
	"textes\\TAL-2008-49-3-01-Adda.txt": {
		"tal": {
			"votes": 3,
			"%_votes": 0.375,
			"sum": 2.2094283867997038,
			"average": 0.7364761289332346
		},
		"litte": {
			"votes": 2,
			"%_votes": 0.25,
			"sum": 1.3505207390583185,
			"average": 0.6752603695291592
		}
	}
}
```

### Export en JSON

Les classes `Classifier`, `KNNClasses` et `TextVec` permettent l'export en JSON simple en utilisant le module standard avec un encodeur personnalisé, `KNNJSONEncoder`. Les fonctions 
`json.dump()`, `json.dumps()`, `json.load()` et `json.loads()` peuvent ainsi être utilisée de la manière suivante :

```python
with open("test.json", mode="w", encoding="utf-8") as f:
	json.dump(classifier, f, ensure_ascii=False, indent='\t', cls=KNNJSONEncoder)
```

### Personnalisation

Il est possible de personnaliser la pipeline de traitement en choisissant des fonctions adaptées.

#### normalisation

Il est possible de fournir une fonction de normalisation qui sera appliquée en pré-traitement de la classification. Le package fournit la fonction `tf_idf()`, mais il est possible
pour l'utilisateur de créer sa propre fonction, tant qu'elle respecte certains critères d'IO :
- Paramètres : `vec: TextVec, classes: dict, vocabs: dict`  
L'objet `TextVec` est le texte à classer, et `classes` est l'ensemble des données des classes ; `vocabs` regroupe, pour chaque classe, la fréquence de chaque terme. Rien n'oblige à 
utiliser tous ces éléments mais ils doivent être présents dans la signature de la fonction.
- Retour : `tuple[dict, dict]`  
La fonction doit retourner un tuple avec le vecteur du texte à classer d'une part (sous la forme d'un dictionnaire ou d'un TextVec), et un dictionnaire des classes avec leurs vecteurs 
d'autre part (ces derniers sous la forme de dictionnaires ou de TextVec).

#### calcul de similarité

L'utilisateur a la possibilité de fournir une fonction de similarité pour la classification. Le package propose déjà `cosVector`, `dice` et `jaccard`.
- Paramètres : `vec1: dict, vec2: dict`  
Les deux vecteurs sont un dictionnaire de tokens, avec 'token' : fréquence
- Retour : `Sim | Dissim`
La fonction doit retourner un objet `Sim` ou `Dissim`, afin que le système sache s'il s'agit de similarité ou de distance. Il suffit de fournir le résultat numérique comme suit :
```python
res = 0.9
return Sim(res)
```

**Note :** il est possible d'utiliser une fonction d'un autre package en l'adaptant à l'aide d'un décorateur :
```python
def sdfgf(func):
	def wrapper(*args, **kwargs):
		# Possibilité de traiter ici les entrées
		return Sim(func(*args, **kwargs))
	return wrapper
	
...

# avec classes: KNNClasses
classes.classify(txt, 3, func=sdfgf(fonction_sim))

```

## Améliorations possibles

- rajouter des **fonctions de normalisation** *built-in*
- rajouter des **fonctions de similarité** *built-in*
- permettre la **parallélisation des traitements**
- intégrer d'autres **algorithmes de classification** (supervisé ou non, comme le CAH) interfacés de la même manière


## Classes

![Classes du package](/uml/classes_KNN.svg "diagramme de classe du package"){: .shadow}


## Pour finir sur une note rigolote 🤪

![Meme](/meme.gif "meme"){: .shadow}