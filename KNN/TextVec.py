# -*- coding: utf-8 -*-

'''
INFORMATIONS GENERALES
=========================================================================
Cours de Python - M1 Sciences du Langage parcours Industries de la Langue
Projet individuel d'algorithmique

INFORMATIONS SUR LE MODULE
=========================================================================
:auteur: Jérémy Bourdillat <Jeremy.Bourdillat@etu.univ-grenoble-alpes.fr>
:version: 1.0

Module hébergeant la classe 'TextVec'.
'''

import re



class TextVec:
    """Classe représentant un texte tokenisé et vectorisé, et permettant la manipulation de ces données.

        :param lbl: le nom du document
        :type lbl: str
        :param vec: le texte (brut, tokenisé et/ou vectorisé)
        :type vec: dict | str
        :param stopwords: liste des tokens à retirer, defaults to None
        :type stopwords: set, optional
        :param hapax: si l'on doit retirer les tokens de fréquence <= 1, defaults to False
        :type hapax: bool, optional
    """
    
    grm = re.compile(r"""
    (?:etc\.|p\.ex\.|cf\.|M\.|MM\.) |
    (\w+['’]? |
    \- |
    [^\s\w]+ |
    \n) 
    """, flags=re.X)

    def __init__(self, lbl: str, vec: dict | list | str, stopwords: set = None, hapax: bool = False):


        self._vector = {}
        self._label = lbl

        match vec:
            case str():
                # Si les données d'entrée sont une chaîne de caractères :

                tokens = self._tokenize(vec, self.grm)

                self._vectorize(tokens)

            case dict():
                # Si les données d'entrée sont un dictionnaire (token -> fréquence) :

                self._vector = vec

            case list():
                # Si les données d'entrée sont une liste de tokens :

                self._vectorize(vec)
                


        self._filter(stopwords, hapax)

    def _tokenize(self, txt: str, regex) -> list[str]:
        """Tokenise la chaîne de caractères à l'aide d'une expression régulière.

        :param txt: le texte brut à tokeniser
        :type txt: str
        :param regex: l'expression régulière de tokenisation
        :type regex: _type_
        :return: une liste de tokens
        :rtype: list[str]
        """
        
        return list(regex.findall(txt))
    pass


    def _vectorize(self, tokens: list[str]):
        """Méthode vectorisant une liste de tokens et stockant le vecteur résultant dans l'objet (agit in-place).

        :param tokens: les tokens à ajouter
        :type tokens: list[str]
        """

        for tok in tokens:
            if tok in self._vector:
                self._vector[tok] += 1
            else:
                self._vector[tok] = 1


    def _filter(self, stopwords: set, hapax: bool):
        """Méthode filtrant les tokens en supprimant ceux présents dans la stoplist (agit in-place).

        :param stopwords: la stoplist
        :type stopwords: set
        :param hapax: si l'on veut supprimer les tokens de fréquence absolue <= 1
        :type hapax: bool
        """

        if stopwords == None and hapax == False:
            return
        
        for token in list(self._vector):
            if stopwords != None and token in stopwords:
                self._vector.pop(token)
                continue

            if hapax != False and self._vector[token] <= 1:
                self._vector.pop(token)




    ############    DUNDER METHODS    ############

    def __repr__(self):
        return str(self.__dict__)
    
    def __iter__(self):
        return iter(self._vector)

    def __getitem__(self, item):
        return self._vector[item]
    
    def __contains__(self, item):
        return item in self._vector
    
    def __len__(self):
        return len(self._vector)
    
    def __json__(self):
        return {"label" : self._label, "vector" : self._vector}

pass
