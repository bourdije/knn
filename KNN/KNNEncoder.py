# -*- coding: utf-8 -*-

'''
INFORMATIONS GENERALES
=========================================================================
Cours de Python - M1 Sciences du Langage parcours Industries de la Langue
Projet individuel d'algorithmique

INFORMATIONS SUR LE MODULE
=========================================================================
:auteur: Jérémy Bourdillat <Jeremy.Bourdillat@etu.univ-grenoble-alpes.fr>
:version: 1.0

Module hébergeant les encodeurs de sérialisation personnalisés pour le package et ses fonctions.
'''


import json



class KNNJSONEncoder(json.JSONEncoder):
    """Implémentation de cette solution : https://discuss.python.org/t/introduce-a-json-magic-method/21768  
    Je cherchais à savoir si une dunder method pouvait permettre de rendre serialisable une classe assez facilement ; malheureusement, ce n'est pas encore le cas
    """

    def default(self, o):

        if callable(getattr(o, "__json__", False)):
            return o.__json__()
        raise TypeError(f'Object of type {o.__class__.__name__} '
                        f'is not JSON serializable')