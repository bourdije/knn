# -*- coding: utf-8 -*-

'''
INFORMATIONS GENERALES
=========================================================================
Cours de Python - M1 Sciences du Langage parcours Industries de la Langue
Projet individuel d'algorithmique

INFORMATIONS SUR LE MODULE
=========================================================================
:auteur: Jérémy Bourdillat <Jeremy.Bourdillat@etu.univ-grenoble-alpes.fr>
:version: 1.0

Module hébergeant la classe 'Classifier'.
'''


import glob
import os
from .KNNClasses import KNNClasses, TextVec, cosVector, Callable
from typing import Iterable
import json



class Classifier:
    """Classe permettant une classification en k plus proches voisins facile et rapide, notamment dans le cas du traitement d'arborescences entières.

    :param descr: la description du système (intégrée par la suite aux résultats)
    :type descr: str
    :param stoplist: liste de tokens à supprimer des vecteurs, defaults to None
    :type stoplist: str | list[str] | set, optional
    :param hapax: si l'on veut supprimer les tokens avec une fréquence absolue de 1 ou moins, defaults to False
    :type hapax: bool, optional
    """

    def __init__(self, descr: str, stoplist: str | list[str] | set = None, hapax: bool = False):


        self._classes = KNNClasses(descr)

        # Création de la stoplist :

        match stoplist:
            case str() if os.path.isfile(stoplist):
                # Si l'on a fourni un nom de fichier :

                with open(stoplist, mode="r", encoding="utf-8") as f:
                    self._stoplist = set(f.read().split('\n'))

            case object() if isinstance(stoplist, Iterable) and all(type(el) is str for el in stoplist):
                # Si l'on a fourni un iterable de tokens :

                self._stoplist = set(stoplist)

            case _:
                self._stoplist = None

        self._hapax = hapax


    def classes_from_directories(self, dirs: str | list[str]):
        """Méthode définissant tous les sous-dossiers de 'dirs' comme des classes et les fichiers qu'ils contiennent comme autant de vecteurs les composant.
        Ajoute les classes au classifieur.

        :param dirs: Un ou plusieurs dossiers dans lequel chercher les classes
        :type dirs: str | list[str]
        """


        if type(dirs) is str:
            dirs = [dirs]

        for directory in dirs:

            folders = [os.path.join(directory, i) for i in os.listdir(directory) if os.path.isdir(os.path.join(directory, i))]
            
            for dir in folders:
                files = glob.glob(os.path.join(dir, "*.txt"))
            
                current_class =os.path.basename(dir) 
                self._classes.add_class(current_class)

                for text in files:
                    with open(text, mode="r", encoding="utf-8") as f:
                        self._classes.add_vector(current_class, TextVec(os.path.basename(text), f.read(), self._stoplist, self._hapax))


    def classes_from_directory(self, dir: str):
        """Méthode définissant le dossier 'dir' comme une classe et les fichiers qu'il contient comme autant de vecteurs la composant.
        Ajoute la classe au classifieur.

        :param dir: Un seul dossier contenant les vecteurs
        :type dir: str
        """

        files = glob.glob(os.path.join(dir, "*.txt"))
    
        current_class =os.path.basename(dir) 
        self._classes.add_class(current_class)

        for text in files:
            with open(text, mode="r", encoding="utf-8") as f:
                self._classes.add_vector(current_class, TextVec(os.path.basename(text), f.read(), self._stoplist, self._hapax))


    def classify_from_files(self, texts: list[str] | str, k: int, method: Callable = cosVector, normalize: Callable = None) -> dict:
        """Méthode permettant la classification en série d'un ou plusieurs textes avec les mêmes paramètres.

        :param texts: un ou plusieurs chemins vers des textes bruts
        :type texts: list[str] | str
        :param method: fonction de similarité à utiliser, defaults to cosVector
        :type method: Callable, optional
        :param normalize: fonction de normalisation à appliquer aux vecteurs avant classification
        :type normalize: Callable, optional
        :return: un dictionnaire des résultats, avec les candidats classés par score
        :rtype: dict
        """
        
        if type(texts) is str:
            texts = [texts]

        results = {'method' : method.__name__, 'k' : k, 'normalization' : normalize.__name__ if normalize != None else None}

        for myst in texts:

            with open(myst, mode="r", encoding="utf-8") as f:
                results[myst] = self._classes.classify(TextVec(myst, f.read(), self._stoplist, self._hapax), k, method, normalize)

        return results


    def classify_from_directories(self, dirs: list[str] | str, k: int, method: Callable = cosVector, normalize: Callable = None) -> dict:
        """Méthode permettant la classification en série d'un ou plusieurs textes avec les mêmes paramètres, à partir de divers dossiers.

        :param texts: un ou plusieurs chemins vers des dossiers contenant des textes bruts
        :type texts: list[str] | str
        :param method: fonction de similarité à utiliser, defaults to cosVector
        :type method: Callable, optional
        :param normalize: fonction de normalisation à appliquer aux vecteurs avant classification
        :type normalize: Callable, optional
        :return: un dictionnaire des résultats, avec les candidats classés par score
        :rtype: dict
        """

        if type(dirs) is str:
            dirs = [dirs]

        results = {'method' : method.__name__, 'k' : k, 'normalization' : normalize.__name__ if normalize != None else None}

        for directory in dirs:
            for myst in glob.glob(os.path.join(directory, "*.txt")):

                with open(myst, mode="r", encoding="utf-8") as f:
                    results[myst] = self._classes.classify(TextVec(myst, f.read(), self._stoplist, self._hapax), k, method, normalize)

        return results
    

    @classmethod
    def load_from_json(cls, s: str):
        input = json.loads(s)

        obj = cls.__new__(cls)
        obj._stoplist = set(input["stoplist"])
        obj._hapax = input["hapax"]


        classes = KNNClasses(input["classes"]["description"])

        for c in input["classes"]["classes"]:
            classes.add_class(c)
            for doc in input["classes"]["classes"][c]:
                classes.add_vector(c, 
                                   TextVec(doc["label"], doc["vector"], None, False)
                                   ) 
                
            

        obj._classes = classes

        return obj    

    ############    PROPERTIES    ############

    @property
    def classes(self) -> KNNClasses:
        return self._classes
    

    @property
    def stoplist(self) -> set:
        return self._stoplist
    

    @property
    def hapax(self) -> bool:
        return self._hapax


    ############    DUNDER METHODS    ############

    def __json__(self):
        return {
            "stoplist" : list(self.stoplist), 
            "hapax" : self.hapax,
            "classes" : self.classes.__json__()
            }
    

    def __repr__(self):
        return str(json.dumps(self.__json__(), ensure_ascii=False, indent='\t'))