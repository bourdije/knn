# -*- coding: utf-8 -*-

'''
INFORMATIONS GENERALES
=========================================================================
Cours de Python - M1 Sciences du Langage parcours Industries de la Langue
Projet individuel d'algorithmique

INFORMATIONS SUR LE PACKAGE
=========================================================================
:auteur: Jérémy Bourdillat <Jeremy.Bourdillat@etu.univ-grenoble-alpes.fr>
:version: 1.0

Ce package permet la classification supevisée de documents textuels par k-ppv (k plus proches voisins).   
Il propose une classe représentant un document sous la forme d'un vecteur, TextVec, et une classe encapsulant les étiquettes attribuables,
les vecteurs qui les composent et les méthodes permettant la classification et la modification des classes existantes. Différentes méthodes de calcul
de similarité sont aussi proposées, comme la similarité cosinus ou bien les indices Jaccard et DICE.  

De plus, une classe 'Classifier' permet de facilement effectuer des classifications en limitant la manipulation des éléments précédemment cités.
'''

from .Classifier import *
from .tools import *