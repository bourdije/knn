# -*- coding: utf-8 -*-

'''
INFORMATIONS GENERALES
=========================================================================
Cours de Python - M1 Sciences du Langage parcours Industries de la Langue
Projet individuel d'algorithmique

INFORMATIONS SUR LE MODULE
=========================================================================
:auteur: Jérémy Bourdillat <Jeremy.Bourdillat@etu.univ-grenoble-alpes.fr>
:version: 1.0

Module contenant la définition de la classe KNNClasses.
'''


from .tools import *
from .TextVec import *
import json
from typing import Callable


class KNNClasses:
    """Permet la représentation de différents labels et leurs textes de référence pour la classification.

    :param description: la description de l'ensemble des classes
    :type description: str
    """

    def __init__(self, description: str):

        self._description = description
        self._classes = {}
        self._vocabs = {}


    def add_class(self, lbl: str):
        """Méthode permettant d'ajouter une classe au système.

        :param lbl: le label de la classe
        :type lbl: str
        """

        self._classes[lbl] = []
        self._vocabs[lbl] = {}

    def add_vector(self, lbl: str, vec: TextVec):
        """Méthode permettant d'ajouter un vecteur à une classe.

        :param lbl: le label de la classe à laquelle rajouter un vecteur
        :type lbl: str
        :param vec: le vecteur à rajouter
        :type vec: TextVec
        """

        self._classes[lbl].append(vec)

        for key in vec._vector.keys():
            if key in self._vocabs[lbl]:
                self._vocabs[lbl][key] += vec._vector[key]
            else:
                self._vocabs[lbl][key] = vec._vector[key]


    def del_class(self, lbl: str):
        """Méthode permettant de supprimer une classe.

        :param lbl: le label de la classe à supprimer
        :type lbl: str
        """

        if lbl in self._classes:
            self._classes.pop(lbl)
            self._vocabs.pop(lbl)
            

    @classmethod
    def load_from_json(cls, s: str) -> 'KNNClasses':
        """Méthode de classe permettant de récupérer un objet depuis une sérialisation JSON.

        :param s: la chaîne à desérialiser
        :type s: str
        :return: l'objet KNNClasses issu du JSON
        :rtype: KNNClasses
        """

        input = json.loads(s)
        obj = cls(input["description"])

        for c in input["classes"]:

            obj.add_class(c)

            obj._classes[c] = [TextVec(i["label"], i["vector"], None, False) for i in input["classes"][c]]

        return obj


    def classify(self, txt: TextVec, k: int, func: Callable = cosVector, normalize: Callable = None) -> dict:
        """Méthode permettant de classer un document parmi les classes présentes dans l'objet en utilisant la méthode des k plus proches voisins

        :param txt: le document à classer
        :type txt: TextVec
        :param k: le nombre de voisins sélectionnés
        :type k: int
        :param func: fonction calculant la similarité entre les textes, defaults to cosVector
        :type func: Callable, optional
        :param normalize: fonction de pré-traitement des vecteurs avant calcul, defaults to None
        :type normalize: Callable, optional
        :return: dictionnaire des résultats sérialisable
        :rtype: dict
        """

        vec = {}
        
        if normalize != None and isinstance(normalize, Callable):
            v, classes = normalize(txt, self._classes, self._vocabs)

            for cls in self._classes:
                vec[cls] = TextVec(txt._label, v[cls])
        else:
            classes = self._classes

            for cls in self._classes:
                vec[cls] = TextVec(txt._label, txt._vector)



        values_sims = []

        for cls in classes:
            # Pour chaque classe, on calcule la similarité entre ses vecteurs et le vecteur d'entrée

            for vector in classes[cls]:

                ret = func(vec[cls], vector)

                # Si la fonction calcule une dissimilarité, on inverse
                if type(ret) is Dissim:
                    values_sims.append((cls, 1 - ret))
                else:
                    values_sims.append((cls, ret))

        values_sims.sort(reverse=True, key=lambda x : x[1])
        nb_votes = len(values_sims)
        values_sims = values_sims[:k]

        vote_counts = {}

        for vote in values_sims:
            if vote[0] in vote_counts:
                vote_counts[vote[0]].append(vote[1])
            else:
                vote_counts[vote[0]] = [vote[1]]

        return dict(
            sorted(
            {key : {'votes' : len(val), '%_votes' : len(val)/nb_votes, 'sum' : sum(val), 'average' : sum(val)/len(val)} for key, val in vote_counts.items()}.items(), 
            key=lambda el : (el[1]['votes'], el[1]['sum']), reverse=True))



    ############    PROPERTIES    ############

    @property
    def description(self):
        return self._description
    
    @property
    def classes(self):
        return self._classes
    

    ############    DUNDER METHODS    ############

    def __repr__(self):
        return str(self.__dict__)
    
    def __json__(self):
        return {
            "description" : self._description, 
            "classes" : {k : [tv.__json__() for tv in v] for (k, v) in self._classes.items()}
            }

pass