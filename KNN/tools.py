from math import sqrt, log

class Dissim(float):
    pass

class Sim(float):
    pass

def cosVector(vec1: dict, vec2: dict) -> Sim:
    '''Retourne le cosinus des deux vecteurs.
    cos(v1, v2) = (v1 ∙ v2) / (∥v1∥ * ∥v2∥) = pdtScalaire(v1, v2) / pdtNormes(v1, v2)
    '''

    v1norm = 0
    v2norm = 0
    scalarProduct = 0

    #On calcule la norme de chaque vecteur, en élevant chaque coordonée au carré pour ensuite faire la racine de la somme, 
    # afin d'obtenir la valeur absolue
    for k in vec1:
        v1norm += vec1[k]**2

        #On calcule le produit scalaire (la somme du produit des coordonnées des vecteurs deux à deux)
        if k in vec2:
            scalarProduct += (vec1[k]*vec2[k])


    for k in vec2:
        v2norm += vec2[k]**2
    
    v1norm = sqrt(v1norm)
    v2norm = sqrt(v2norm)


    # On vérifie que l'on n'a pas un dénominateur nul (sans quoi le calcul retournerait une erreur)
    if (v1norm*v2norm) != 0:
        return Sim(abs(scalarProduct / (v1norm*v2norm)))
    else:
        return Sim(0)
pass


def dice(vec1: dict, vec2: dict) -> Sim:
    '''Retourne l'indice DICE de similarité entre les deux listes :
    s(vec1, vec2) = 2(vec1 ∩ vec2) / (vec1 + vec2)
    Avec vec1 et vec2 débarassés des doublons
    '''

    size_a = len(vec1)
    size_b = len(vec2)
    intersect = len(set(vec1) & set(vec2))

    if size_a+size_b != 0:
        return Sim((2*intersect)/(size_a+size_b))
    else:
        return Sim(0)

pass

def jaccard(vec1: dict, vec2: dict) -> Sim:
    '''Retourne l'indice jaccard de similarité entre les deux listes :
    J(vec1, vec2) = (vec1 ∩ vec2) / (vec1 ∪ vec2)
    Avec vec1 et vec2 débarassés des doublons
    '''

    size_a = len(vec1)
    size_b = len(vec2)
    intersect = len(set(vec1) & set(vec2))

    if (size_a+size_b-intersect) != 0:
        return intersect/(size_a+size_b-intersect)
    else:
        return 0

pass



def tf_idf(vec, classes: dict, vocabs: dict) -> tuple[dict, dict]:

    classes_out = {}
    vec_out = {}

    for cls in classes:
        classes_out[cls] = []
        vec_out[cls] = {}

        for doc in classes[cls]:

            classes_out[cls].append({})

            for token in vec._vector.keys():
                # On incorpore la fréquence du document à classer dans les fréquences de chaque classe

                if token in vocabs[cls]:
                    vocabs[cls][token] += vec._vector[token]
                else:
                    vocabs[cls][token] = vec._vector[token]

                if token in doc:
                    classes_out[cls][-1][token] = doc[token] / log(1+vocabs[cls][token])
        
                if token in vec._vector:
                    vec_out[cls][token] = vec._vector[token] / log(1+vocabs[cls][token])

    return vec_out, classes_out

        