import sys

sys.path.append('..')

from KNN import *
import json
from KNN.KNNEncoder import KNNJSONEncoder


"""Fichier de test des fonctionnalités de base
"""

if __name__ == "__main__":

    ##### POUR CREER UN CLASSIFIEUR DE ZERO :
    
    stoplist = "STOPLIST.txt"
    classifier = Classifier("Test de classification en k-ppv.", stoplist=stoplist, hapax=True)

    classifier.classes_from_directories("../textes")


    ##### POUR CHARGER UN CLASSIFIEUR DEPUIS UN FICHIER JSON :

    with open("test.json", mode="r", encoding="utf-8") as f:
        classifier = Classifier.load_from_json(f.read())

    print(classifier)

    results = classifier.classify_from_directories("../textes", 5, cosVector, normalize=tf_idf)


    ##### SORTIES EN JSON : 

    with open("test.json", mode="w", encoding="utf-8") as f:
        json.dump(classifier, f, ensure_ascii=False, indent='\t', cls=KNNJSONEncoder) # utilisation de l'encodeur personnalisé OBLIGATOIRE
    
    with open("test_results.json", mode="w", encoding="utf-8") as f:
        json.dump(results, f, ensure_ascii=False, indent='\t')

    print()